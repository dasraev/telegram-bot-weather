import telebot
import requests
import pprint
from datetime import datetime
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton,ReplyKeyboardMarkup
bot = telebot.TeleBot('1862034935:AAHntamDbLHUZHgHFozUT3oxHRJstULVPUM')

now = datetime.now()
weekdays = {"Monday":"Dushanba","Tuesday":"Seshanba","Wednesday":"Chorshanba","Thursday":"Payshanba","Friday":"Juma","Saturday":"Shanba","Sunday":"Yakshanba"}
x=now.strftime('%A')
for i in weekdays:
    if i==x:
        x = weekdays[i]
def gen_repmarkup():
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add('📍Joylashuvni jo\'natish')
    return markup

def gen_markup():
    markup = InlineKeyboardMarkup()
    markup.row_width = 3
    markup.add(
        InlineKeyboardButton("o'zbek",callback_data='cb_ozbek'),
        InlineKeyboardButton("russian",callback_data='cb_russian'),
        InlineKeyboardButton("english",callback_data='cb_english')
    )
    return markup

@bot.callback_query_handler(func=lambda call:True)
def callback_query(call):
    if call.data=='cb_ozbek':
        bot.send_message(call.from_user.id,"O'zbek tili tanlandi",reply_markup=gen_repmarkup())
        bot.send_message(call.from_user.id,f"Salom <b>{call.from_user.first_name}</b> ob-havo ma'lumotlarini bilish uchun"
                                           "\n📍Joylashuvni jo'natish tugmasini bosing "
                                           "Joylashuvni jo'natishdan oldin qurilmangizdagi gpsni yoqing",parse_mode='html')

@bot.message_handler(commands=['start'])
def message_handler(message):
    bot.send_message(message.chat.id,'Tilni tanlang',reply_markup=gen_markup())

@bot.message_handler(content_types=['location'])
def message(location):
    url_location = f"http://api.openweathermap.org/geo/1.0/reverse?lat={location.location.latitude}&lon={location.location.longitude}&appid=676bed06e1d7e1f10207a500671530f7"
    url_weather = f"https://api.openweathermap.org/data/2.5/onecall?lat={location.location.latitude}&lon={location.location.longitude}&exclude=minutely,hourly&units=metric&appid=676bed06e1d7e1f10207a500671530f7"
    data_weather = requests.get(url_weather).json()
    data_location = requests.get(url_location).json()
    if data_location[0]['country'] == "UZ":
        data_location[0]['country']='Uzbekistan'
    bot.send_message(location.from_user.id,f"<i>⏳Sana:{datetime.fromtimestamp(data_weather['current']['dt']).strftime('%d.%m, %H:%M,')} {x}</i>\n"
                                           f"<i>🌆Shahar:{ data_location[0]['country']}, {data_location[0]['name']}</i>"
                                           f"\n\n☀️"
                                           f"\n<b>Hozir</b>:<i>{data_weather['current']['temp']}°C</i>"
                                           f"\n<b>Tong</b>:<i>{data_weather['daily'][0]['temp']['morn']}°C</i>"
                                           f"\n<b>Kunduzi</b>:<i>{data_weather['daily'][0]['temp']['day']}°C</i>"
                                           f"\n<b>Kechasi</b>:<i>{data_weather['daily'][0]['temp']['eve']}°C</i>"
                                           f"\n<b>Tun</b>:<i>{data_weather['daily'][0]['temp']['night']}°C</i>"
                                           f"\n\nShamol</b>:<i>{data_weather['current']['wind_speed']}m/s</i>"
                                           f"\n<b>☁Bulutlilik</b>:<i>{data_weather['current']['clouds']}</i>"
                                           f"\n\n<b>🌅Quyosh chiqishi</b>: <i>{datetime.fromtimestamp(data_weather['daily'][0]['sunrise']).strftime('%d.%m, %H:%M,')} {x}</i>"
                                           f"\n<b>🌄Quyosh botishi</b>: <i>{datetime.fromtimestamp(data_weather['daily'][0]['sunset']).strftime('%d.%m, %H:%M,')} {x}</i>",parse_mode='html'
                     )



bot.polling(none_stop=True)


